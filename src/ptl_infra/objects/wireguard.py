# https://www.procustodibus.com/blog/2022/10/wireguard-in-podman/#use-for-site-masquerading

from tarfile import TarInfo

PEERS = {
    't7K0C9dPL56wMSGcb7o52ym9YFTa5yml1FNinxWXHRU=': '10.10.0.2/32',  # marv@ares
    'FnKLnR9JMiHM0Z/nHDKiYOzAt0SRzTxKuyGo4D1ltCc=': '10.10.0.3/32',  # mborges@mega-garlic
    'eiACLhjorqmy1U5U5dJq2Kpqzmc42WcWlzO5HMGaW38=': '10.10.0.4/32',  # pherjung@silvretta
    'ol7iBqc1p3Jexc5KSmlP5IGspt8tdOAxyM5zflrHBAU=': '10.10.0.5/32',  # marv@fp3
    's6Di4qejxbE1lxhE0ToWETPJoCQqA/xjLzOkriag+Ao=': '10.10.0.6/32',  # marv@lyotard
    'Qs426H1FRqvzoOUCQfoMWMA0mPR/GVS7P7vX/L5he0w=': '10.10.0.7/32',  # coniferous-cube@stampeding-dodo
    '0OtPrXtHBBUuFwrr7TkM9qW4tIOtOsfLjaffYMIuGUE=': '10.10.0.8/32',  # sinux@convenient-poko
    'kiwFca8QK+AGy0wUnTPD2vDIc9tWU4vr33jcE9vy+RU=': '10.10.0.9/32',  # mborges@heavy-tomato
    'LEhNFwBxj/u1gVIY7xwEPe6v9pnwBlnf18WwNDv3tgo=': '10.10.0.10/32',  # mborges@spiny-tomato
    'a13QhIN4SLecdpI7dc57WC81W0JkfjLH/jEQH+naCG4=': '10.10.0.11/32',  # bow@ptl
    'ZpoFPx83owegpCw1dzC/73SYXWFnD85DMFsc+JITuwI=': '10.10.0.12/32',  # marv@lite
    'B7pyMnXRFVymCE0nVkMUYIaeDnbcoJwaD2pPMaaSMkM=': '10.10.0.13/32',  # marv@zeus
    'xAQGtQeMaSw8bPl7lQrcgcND067CF21TO3cysfE8A3A=': '10.10.0.14/32',  # mic@micbookpro
    '6/3chAbsY8eFjwm7aGhAFIDeAVbQxmjqp9aI1TymbBk=': '10.10.0.15/32',  # dji
    'InIK55ZMyy6TC7bYhqQDejbSlWtBUi+JYx0PTARQvTI=': '10.10.0.16/32',  # marv@b1
    'Vu+OPzGW6713/wphiF1wiBevc3f2aKKwscxHUTQz6nw=': '10.10.0.17/32',  # mborges@teletruc
}


def generate() -> dict[TarInfo, str]:
    wg_set = 'wg set %i private-key /etc/wireguard/wg0.key'
    iptables_append = 'iptables --table nat --append POSTROUTING ! --out-interface %i --jump MASQUERADE'
    conf = '\n'.join(
        (
            '[Interface]',
            'ListenPort = 51820',
            'Address = 10.10.0.1/24',
            f'PostUp =  {wg_set} && {iptables_append}',
            '',
            *(
                line
                for public_key, allowed_ips in PEERS.items()
                for line in (
                    '[Peer]',
                    f'PublicKey = {public_key}',
                    f'AllowedIPs = {allowed_ips}',
                    '',
                )
            ),
        ),
    )
    return {
        TarInfo('/etc/wireguard/wg0.conf'): conf,
    }
