import json
import sys
import urllib.parse
from pathlib import Path

import pypxe.server as pxe  # type: ignore[import-untyped]
import requests

_HOSTNAME = 'bebop'
_KERNEL = 'kernel-x86_64'
_INITRAMFS = 'initramfs.x86_64.img'
_ROOTFS = 'rootfs.x86_64.img'
_IGNITION = 'config.ign'


def main() -> None:
    ip_address = sys.argv[1]
    sys.argv = [sys.argv[0]]

    netboot = Path(pxe.SETTINGS['NETBOOT_DIR'])
    netboot.mkdir(parents=True, exist_ok=True)

    def download(filename: str, url: str) -> None:
        path = netboot / filename
        if path.exists():
            return
        with requests.get(url, stream=True, timeout=5) as response:
            response.raise_for_status()
            with path.open('wb') as file:
                for chunk in response.iter_content(chunk_size=4096 * 1024):
                    file.write(chunk)

    download('chainload.kpxe', 'http://boot.ipxe.org/undionly.kpxe')
    for filename in (_KERNEL, _INITRAMFS, _ROOTFS):
        version = '38.20230430.3.1'
        url_prefix = f'https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/{version}/x86_64/fedora-coreos-{version}-live-'
        url = f'{url_prefix}{filename}'
        download(filename, url)

    boot = netboot / 'boot.http.ipxe'
    boot.write_text(
        '\n'.join(
            (
                '#!ipxe',
                ' '.join(
                    (
                        'kernel',
                        f'http://{ip_address}/{_KERNEL}',
                        f'initrd={_INITRAMFS}',
                        f'coreos.live.rootfs_url=http://{ip_address}/{_ROOTFS}',
                        'coreos.inst.install_dev=/dev/sda',
                        f'coreos.inst.ignition_url=http://{ip_address}/{_IGNITION}',
                    ),
                ),
                ' '.join(
                    (
                        'initrd',
                        f'http://{ip_address}/{_INITRAMFS}',
                    ),
                ),
                'boot',
            ),
        ),
    )

    def file_contents(contents: str) -> dict[str, str]:
        quoted = urllib.parse.quote(contents, safe='')
        return dict(
            source=f'data:,{quoted}',
        )

    config_text = json.dumps(
        dict(
            ignition=dict(
                version='3.3.0',
            ),
            passwd=dict(
                users=[
                    dict(
                        name='root',
                        passwordHash='$y$j9T$bICSDtpzpN3HYHLcd0WUa1$lwlTDo.2Z3mlxacoVhvzTTqtSb82UaTufwm81btMYn0',
                    ),
                ],
            ),
            storage=dict(
                files=[
                    dict(
                        path='/root/.config/sops/age/keys.txt',
                        contents=file_contents(Path('keys.txt').read_text('UTF-8')),
                        # u=rw,go=
                        mode=0o600,
                    ),
                ],
            ),
            systemd=dict(
                units=[
                    dict(
                        # auto-update agent is incompatible with rpm-ostree rebase
                        name='zincati.service',
                        enabled=False,
                    ),
                    dict(
                        name='rpm-ostree-rebase.service',
                        enabled=True,
                        contents="""[Install]
WantedBy=default.target

[Unit]
ConditionFirstBoot=true
Before=first-boot-complete.target
After=ignition-firstboot-complete.service
After=network-online.target
Wants=network-online.target

[Service]
Type=oneshot
ExecStart=rpm-ostree rebase ostree-unverified-registry:registry.gitlab.com/posttenebraslab/ptl-infra:latest
ExecStart=rpm-ostree install btrbk ipa-client libvirt qemu virt-install
ExecStart=systemd-run --no-block --property=After=first-boot-complete.target systemctl reboot
""",
                    ),
                ],
            ),
        ),
    )
    config_file = netboot / _IGNITION
    config_file.write_text(config_text)

    pxe.SETTINGS['USE_IPXE'] = True
    pxe.SETTINGS['DHCP_MODE_PROXY'] = True
    pxe.SETTINGS['DHCP_FILESERVER'] = ip_address
    pxe.SETTINGS['DHCP_SERVER_IP'] = ip_address
    pxe.SETTINGS['USE_HTTP'] = True
    pxe.main()


if __name__ == '__main__':
    main()
