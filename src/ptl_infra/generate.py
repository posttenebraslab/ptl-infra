# ruff: noqa: T201 (print)

import importlib
import io
import pkgutil
import sys
import tarfile

from . import objects


def main() -> int:
    with tarfile.open('overlay.tar.xz', 'w:xz') as archive:
        for _, name, _ in pkgutil.iter_modules(objects.__path__):
            module_name = f'{objects.__name__}.{name}'
            module = importlib.import_module(module_name)
            documents = module.generate()
            for tarinfo, content in documents.items():
                print(tarinfo.name)
                content_bytes = content.encode('UTF-8')
                tarinfo.size = len(content_bytes)
                fileobj = io.BytesIO(content_bytes)
                archive.addfile(tarinfo=tarinfo, fileobj=fileobj)

    return 0


if __name__ == '__main__':
    sys.exit(main())
