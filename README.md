# ptl-infra

## Setup

- Install podman

    `$PACKAGE_MANAGER install podman`

- Create a venv

    `python -m venv .venv`

- Install Python package dependencies

    `.venv/bin/pip install --constraint requirements-dev.txt --editable '.[dev]'`

## Build

This build is also run by GitLab CI on every push and daily.

- Create host base image

    `podman build --tag registry.gitlab.com/posttenebraslab/ptl-infra:latest .`

- Push host base image to registry

    `podman push registry.gitlab.com/posttenebraslab/ptl-infra:latest`

### Build Process

The build [Containerfile](Containerfile) simply adds to the CoreOS base image:

- the content of the `static` directory, owned by root with mode `u=rw,go=`
- the content of the generated `overlay.tar.xz` archive, with ownership and mode from the archive

The peculiar CoreOS [filesystem layout](https://docs.fedoraproject.org/en-US/fedora-coreos/storage/#_mounted_filesystems) makes it complicated to provision files in `/root`, `/opt`, `/var`, `/run`. The [recommended workaround](https://coreos.github.io/rpm-ostree/container/#dealing-with-opt) is to use [tmpfiles.d](https://www.freedesktop.org/software/systemd/man/latest/tmpfiles.d.html), but it does not cleanup removed entries (although `/run` is cleaned up on boot).

## Full install

- Run the pxe server. Replace `<ip>` in the following command with the IP on your computer network interface

    `sudo .venv/bin/python -m ptl_infra.pxe <ip>`

- Boot the server in PXE mode, it will reinstall the system

## Upgrade

- The server does this daily, but you can do it immediately:

    `sudo rpm-ostree upgrade --reboot`

## CI/CD

GitLab CI builds a new host base image on every push: [.gitlab-ci.yml](.gitlab-ci.yml)

## Requirements upgrade

```
.venv/bin/pip-compile --output-file=requirements.txt --strip-extras --upgrade
.venv/bin/pip-compile --extra=dev --output-file=requirements-dev.txt --strip-extras --upgrade
```

## Secrets

Ask an admin for the `keys.txt` file containing the private AGE key.

- Encrypt a new file:

```
SOPS_AGE_KEY_FILE="keys.txt" sops -i -e static/etc/matrix-sliding-sync/secret
```

with podman instead:

```
podman run --rm --interactive --tty --env SOPS_AGE_KEY="$(<keys.txt)" --volume "$PWD":"$PWD":Z --workdir "$PWD" quay.io/getsops/sops:v3.8.1 -e static/etc/wireguard/wg0.key
```

- Edit an encrypted file

```
SOPS_AGE_KEY_FILE="keys.txt" sops static/etc/matrix-sliding-sync/secret
```

with podman instead:
```
podman run --rm --interactive --tty --env SOPS_AGE_KEY="$(<keys.txt)" --volume "$PWD":"$PWD":Z --workdir "$PWD" quay.io/getsops/sops:v3.8.1 static/etc/wireguard/wg0.key
```

## Email

The email setup is a bit peculiar:

* Incoming mail arrives on our `MX` server `mail.posttenebraslab.ch:25`
* Router redirects this to the `Stalwart` mail server listening on `bebop.lan.posttenebraslab.ch:25`
* Mail for the `comite` account is forwarded to the comitee members by the `ptl-jmap.py` script.
* `Stalwart` sends outgoing mail through the `10.42.0.25` IP address instead of the default route.
* Router redirects outgoing traffic from that address through the `mail.posttenebraslab.ch` IP address.
