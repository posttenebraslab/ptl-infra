posttenebraslab.ch {
	reverse_proxy /_matrix/client/unstable/org.matrix.msc3575/sync systemd-matrix-sliding-sync:8008
	handle /.well-known/matrix/client {
		header Content-Type application/json
		respond <<JSON
		{
			"m.homeserver": {
				"base_url": "https://posttenebraslab.ch"
			},
			"m.identity_server": {
				"base_url": "https://vector.im"
			},
			"org.matrix.msc3575.proxy": {
				"url": "https://posttenebraslab.ch"
			}
		}
		JSON 200
	}

	@matrix path /.well-known/matrix/* /_matrix/*
	handle @matrix {
		reverse_proxy systemd-matrix-synapse:8008
	}

	handle {
		redir https://www.posttenebraslab.ch{uri} permanent
	}
}

www.posttenebraslab.ch {
	header {
		Strict-Transport-Security "max-age=31536000;"
		# wiki break galore
		# Content-Security-Policy "default-src 'self'; frame-src cloud.posttenebraslab.ch; script-src 'self' 'unsafe-inline';"
		X-Frame-Options "SAMEORIGIN"
		X-Content-Type-Options "nosniff"
	}

	root * /srv/ptl-web
	file_server

	redir /status /status/ permanent
	handle_path /status/* {
		reverse_proxy systemd-ptl-status:8000
	}

	redir /wiki /wiki/ permanent
	reverse_proxy /wiki/* systemd-dokuwiki
	redir /events /wiki/events/start
}

# https://federationtester.matrix.org/#posttenebraslab.ch
posttenebraslab.ch:8448 {
	reverse_proxy systemd-matrix-synapse:8008
}

cloud.posttenebraslab.ch {
	@proxied not path /login /s/* /public.php/*
	reverse_proxy @proxied systemd-nextcloud
}

mta-sts.posttenebraslab.ch {
	reverse_proxy /.well-known/mta-sts.txt systemd-mail-stalwart:8080
}

ptl.xyz www.ptl.xyz {
	redir https://www.posttenebraslab.ch{uri}
}

arduinodays.ptl.xyz {
	redir https://www.posttenebraslab.ch/wiki/events/2024/03/arduino_days
}

15ans.ptl.xyz {
	redir https://www.posttenebraslab.ch/wiki/events/2024/11/15_ans_ptl
}

ptlino.com ptl-ino.com www.ptlino.com www.ptl-ino.com {
	redir https://www.posttenebraslab.ch/wiki/projects/electronics/ptl-ino
}

(lan-no-tls) {
	@wan not remote_ip private_ranges
	abort @wan
}
(lan) {
	import lan-no-tls
	tls /etc/letsencrypt/live/lan.posttenebraslab.ch/fullchain.pem /etc/letsencrypt/live/lan.posttenebraslab.ch/privkey.pem
}

http://status.lan.posttenebraslab.ch {
	import lan-no-tls
	reverse_proxy systemd-ptl-status:8000
}

cloud.lan.posttenebraslab.ch {
	import lan
	redir /.well-known/carddav /remote.php/dav permanent
	redir /.well-known/caldav /remote.php/dav permanent
	reverse_proxy systemd-nextcloud
}

freeipa.lan.posttenebraslab.ch {
	import lan
	reverse_proxy systemd-freeipa
}

grafana.lan.posttenebraslab.ch {
	import lan
	reverse_proxy systemd-o11y-grafana:3000
}

homeassistant.lan.posttenebraslab.ch {
	import lan
	reverse_proxy systemd-homeassistant:8123
}

ptl-admin.lan.posttenebraslab.ch {
	import lan
	reverse_proxy systemd-ptl-admin-auth
}

drinkingbuddy.lan.posttenebraslab.ch {
	import lan
	reverse_proxy systemd-ptl-drinkingbuddy:5000
}

mail.lan.posttenebraslab.ch {
	import lan
	reverse_proxy systemd-mail-stalwart:8080
}

roundcube.lan.posttenebraslab.ch {
	import lan
	reverse_proxy systemd-mail-roundcube
}
