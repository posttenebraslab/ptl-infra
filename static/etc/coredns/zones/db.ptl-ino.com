$TTL	5m
$ORIGIN ptl-ino.com.

;;;; authority ;;;;
@	SOA posttenebraslab.ch. hostmaster.posttenebraslab.ch. (
	2024123002	; serial
	5m	; refresh
	5m	; retry
	1w	; expiry
	5w	; minimum
)
@	NS	posttenebraslab.ch.
@	NS	fixme.ch.

@	A	85.195.210.243  ; https://serverfault.com/a/613830
www	CNAME	@
