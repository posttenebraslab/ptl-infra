$TTL	5m
$ORIGIN	posttenebraslab.ch.

;;;; authority ;;;;
@	SOA posttenebraslab.ch. hostmaster.posttenebraslab.ch. (
	2024123002	; serial
	5m	; refresh
	5m	; retry
	1w	; expiry
	5w	; minimum
)
@	NS	posttenebraslab.ch.
@	NS	fixme.ch.
@	TXT	"google-site-verification=myQ4Q1LPKker7TNWAiT7Rmj6qsQQdocYn_Yo3XQvHdQ"

;;;; e-mail ;;;;
@	MX	10 mail.posttenebraslab.ch.
ed25519._domainkey	TXT	"v=DKIM1; k=ed25519; h=sha256; p=fNhodOgyrEGUFrs7/7Fm5Ww+cVEte+Ew4qTJjap3ZJM="
rsa._domainkey	TXT	"v=DKIM1; k=rsa; h=sha256; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyePzP9nSnxbqm08P16wXmoMeN4FBF6pCQ66JAiUIqb5/0HhUOHBQlueupy7pqMx7SDUqO8LsqqS2sPRGq5SbaGqhTsw38UYX2SeH68PWUNjbwMzSXaVYOh6yo3Rpq/vo1LJO5MFWLwUHcFoDxYovwJcD2/ZMKD5dpYloilPPSixKazQQIeKTZjC" "ichp1KuRxGM6nB1h95G1w7A20YHEpofQ6jhLQ4lL6whgnwNeZGnAlIbS6usaQ9GxGvT8BvFJ9ZBesTVbZ3jXsCcqS5oLD1veds/Zafe8LSINKbatZlJ+v22Pg7MV5LpzwmBajiqPBP8d8SCVu8pPrPkKsQcjLQwIDAQAB"
mail	TXT	"v=spf1 a ra=postmaster -all"
@	TXT	"v=spf1 mx ra=postmaster -all"
mta-sts	CNAME	@
_mta-sts	TXT	"v=STSv1; id=12482865779382387954"
_dmarc	TXT	"v=DMARC1; p=reject; rua=mailto:postmaster@posttenebraslab.ch; ruf=mailto:postmaster@posttenebraslab.ch"
_smtp._tls	TXT	"v=TLSRPTv1; rua=mailto:postmaster@posttenebraslab.ch"

;;;; kerberos ;;;;
_kerberos-master._tcp	SRV	0 100 88 freeipa.lan.posttenebraslab.ch.
_kerberos-master._udp	SRV	0 100 88 freeipa.lan.posttenebraslab.ch.
_kerberos._tcp	SRV	0 100 88 freeipa.lan.posttenebraslab.ch.
_kerberos._udp	SRV	0 100 88 freeipa.lan.posttenebraslab.ch.
_kerberos	TXT	"POSTTENEBRASLAB.CH"
_kerberos	URI	0 100 "krb5srv:m:tcp:freeipa.lan.posttenebraslab.ch."
_kerberos	URI	0 100 "krb5srv:m:udp:freeipa.lan.posttenebraslab.ch."
_kpasswd._tcp	SRV	0 100 464 freeipa.lan.posttenebraslab.ch.
_kpasswd._udp	SRV	0 100 464 freeipa.lan.posttenebraslab.ch.
_kpasswd	URI	0 100 "krb5srv:m:tcp:freeipa.lan.posttenebraslab.ch."
_kpasswd	URI	0 100 "krb5srv:m:udp:freeipa.lan.posttenebraslab.ch."
_ldap._tcp	SRV	0 100 389 freeipa.lan.posttenebraslab.ch.
ipa-ca	A	10.42.0.10

;;;; ip ;;;;
@	A	85.195.210.243	; matches reverse DNS
ns1	A	85.195.210.243
mail	A	82.197.186.97	; matches reverse DNS
ns2	A	82.197.186.97

;;;; others ;;;;
cloud	CNAME	@
www	CNAME	@
