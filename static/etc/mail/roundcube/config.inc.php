<?php
$host = 'ssl://systemd-mail-stalwart';
$conn_options = [
        'ssl' => [
                'allow_self_signed' => true,
                'verify_peer_name' => false,
        ],
];

$config['imap_host'] = $host;
$config['imap_conn_options'] = $conn_options;
$config['smtp_host'] = $host;
$config['smtp_conn_options'] = $conn_options;
$config['managesieve_host'] = $host;
$config['managesieve_conn_options'] = $conn_options;
?>
