#!/usr/bin/env python

# Flagged mails are forwarded to comitee members
# Mails marked as junk are trained and moved to the junk mailbox
# Mails marked as nonjunk are trained and moved to the inbox mailbox

# ruff: noqa: T201 (print)
# ruff: noqa: S310 (urllib)

import base64
import itertools
import json
import os
import urllib.parse
import urllib.request
from collections.abc import Iterable

rcpt_tos = os.environ['RCPT_TOS'].split(',')
base_url = os.environ['BASE_URL']
jmap_username = os.environ['JMAP_USERNAME']
jmap_password = os.environ['JMAP_PASSWORD']
train_username = os.environ['TRAIN_USERNAME']
train_password = os.environ['TRAIN_PASSWORD']

keywords_flagged = ('$flagged', '$MailFlagBit0', '$MailFlagBit1', '$MailFlagBit2')
keywords_junk = ('$junk', 'junk', 'Junk')
keywords_notjunk = ('$notjunk', 'nonjunk', 'NonJunk')


def get_headers(username: str, password: str) -> dict[str, str]:
    authorization = base64.b64encode(f'{username}:{password}'.encode()).decode()
    return {'Authorization': f'Basic {authorization}'}


jmap_headers = get_headers(jmap_username, jmap_password)
train_headers = get_headers(train_username, train_password)


def json_request(path: str, data: dict | None = None) -> dict:
    url = f'{base_url}{path}'

    if data:
        data_str = json.dumps(data)
        data_bytes = data_str.encode()
    else:
        data_bytes = None

    request = urllib.request.Request(url=url, data=data_bytes, headers=jmap_headers)
    response = urllib.request.urlopen(request)

    charset = response.info().get_param('charset') or 'utf-8'
    response_str = response.read().decode(charset)
    return json.loads(response_str)


def jmap(*calls: list) -> list:
    data = {
        'methodCalls': calls,
    }

    responses = json_request('jmap/', data)['methodResponses']
    errors = [response for response in responses if response[0] == 'error']
    if errors:
        raise ValueError(errors)

    return responses


def jprint(obj: dict) -> None:
    print(json.dumps(obj))


session = json_request('.well-known/jmap')
get_limit = session['capabilities']['urn:ietf:params:jmap:core']['maxObjectsInGet']
account_id = next(iter(session['accounts'].keys()))

mailboxes, identities = (
    response[1]['list']
    for response in jmap(
        ['Mailbox/get', {'accountId': account_id, 'ids': None}, 'Mailbox'],
        ['Identity/get', {'accountId': account_id, 'ids': None}, 'Identity'],
    )
)

mailbox_roles = {mailbox['role']: mailbox['id'] for mailbox in mailboxes}
mailbox_inbox = mailbox_roles['inbox']
mailbox_junk = mailbox_roles['junk']
identity = identities[0]
identity_id = identity['id']
identity_email = identity['email']
print(f'{account_id=} {mailbox_inbox=} {mailbox_junk=} {identity_id=} {identity_email=}')

condition_is_flagged = {'operator': 'OR', 'conditions': [{'hasKeyword': keyword} for keyword in keywords_flagged]}
condition_is_junk = {'operator': 'OR', 'conditions': [{'hasKeyword': keyword} for keyword in keywords_junk]}
condition_is_notjunk = {'operator': 'OR', 'conditions': [{'hasKeyword': keyword} for keyword in keywords_notjunk]}
query_filters = {
    'forward': condition_is_flagged,
    'junk': condition_is_junk,
    'notjunk': condition_is_notjunk,
}
query_calls = tuple(
    itertools.chain.from_iterable(
        (
            [
                'Email/query',
                {'accountId': account_id, 'filter': query_filter, 'limit': get_limit},
                f'{query_name}/query',
            ],
            [
                'Email/get',
                {
                    'accountId': account_id,
                    '#ids': {'resultOf': f'{query_name}/query', 'name': 'Email/query', 'path': '/ids'},
                    'properties': ['blobId'],
                },
                f'{query_name}/get',
            ],
        )
        for query_name, query_filter in query_filters.items()
    ),
)

envelope = {
    'mailFrom': {'email': identity['email']},
    'rcptTo': [{'email': rcpt_to} for rcpt_to in rcpt_tos],
}
update_clear_flagged = {f'keywords/{keyword}': None for keyword in keywords_flagged}
update_clear_junk = {f'keywords/{keyword}': None for keyword in keywords_junk} | {'mailboxIds': {mailbox_junk: True}}
update_clear_notjunk = {f'keywords/{keyword}': None for keyword in keywords_notjunk} | {
    'mailboxIds': {mailbox_inbox: True},
}


def train(emails: list, train: str) -> Iterable[int]:
    for email in emails:
        email_id = email['id']
        blob_id = email['blobId']

        try:
            url = f'{base_url}jmap/download/{account_id}/{blob_id}/'
            request = urllib.request.Request(url=url, headers=jmap_headers)
            response = urllib.request.urlopen(request)

            data = response.read()

            url = f'{base_url}api/sieve/train?train={train}'
            request = urllib.request.Request(url=url, data=data, headers=train_headers)
            response = urllib.request.urlopen(request)
            response.read()
        except OSError as e:
            print(email_id, blob_id, e)

        yield email_id


def step() -> None:
    while True:
        responses = {response[2]: response[1] for response in jmap(*query_calls)}
        forward_email_ids = responses['forward/query']['ids']
        junk_emails = responses['junk/get']['list']
        notjunk_emails = responses['notjunk/get']['list']
        if not (forward_email_ids or junk_emails or notjunk_emails):
            print('break')
            break

        junk_email_ids = train(junk_emails, 'spam')
        notjunk_email_ids = train(notjunk_emails, 'ham')

        forward_create = {
            email_id: {'identityId': identity_id, 'emailId': email_id, 'envelope': envelope}
            for email_id in forward_email_ids
        }
        forward_update = {email_id: update_clear_flagged for email_id in forward_email_ids}
        junk_update = {email_id: update_clear_junk for email_id in junk_email_ids}
        notjunk_update = {email_id: update_clear_notjunk for email_id in notjunk_email_ids}

        responses = jmap(
            [
                'EmailSubmission/set',
                {'accountId': account_id, 'create': forward_create, 'onSuccessUpdateEmail': forward_update},
                'forward',
            ],
            ['Email/set', {'accountId': account_id, 'update': junk_update}, 'junk'],
            ['Email/set', {'accountId': account_id, 'update': notjunk_update}, 'notjunk'],
        )
        for response in responses:
            method_name, arguments, call_id = response
            match method_name:
                case 'Email/set':
                    updated = arguments.get('updated')
                    if updated:
                        print(f'{call_id} {len(updated)}')
                case 'EmailSubmission/set':
                    created = arguments.get('created')
                    if created:
                        print(f'{call_id} {len(created)}')
        print('continue')


url = f'{base_url}jmap/eventsource?types=Email'
request = urllib.request.Request(url, headers=jmap_headers)
response = urllib.request.urlopen(request)
while True:
    step()
    while True:
        line = response.readline()
        if line == b'\n':
            break
